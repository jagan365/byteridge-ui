﻿import { Routes, RouterModule } from '@angular/router';
import { AuditComponent } from './audit';
import { DashboardComponent } from './dashboard';

import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { RegisterComponent } from './register';
import { AuthGuard } from './_helpers';
import { AuditGuard } from './_helpers/audit.guard';

const routes: Routes = [
    { path: 'dashboard/audit', component: AuditComponent, canActivate: [AuditGuard] },
    { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
    { path: '', component: HomeComponent, canActivate: [AuthGuard] },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },

    // otherwise redirect to home
    // { path: '**', redirectTo: 'home' }
];

export const appRoutingModule = RouterModule.forRoot(routes);