﻿import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';

import { User } from '@/_models';
import { AuthenticationService } from '@/_services';
import { AuditService } from '@/_services/audit.service';

@Component({ templateUrl: 'audit.component.html' })
export class AuditComponent implements OnInit {
    currentUser: User;
    auditsList = [];
    currentPage = 1;

    constructor(
        private authenticationService: AuthenticationService,
        private auditService: AuditService
    ) {
        this.currentUser = this.authenticationService.currentUserValue;
    }

    ngOnInit() {
        this.loadAllAudits();
    }

    // deleteUser(id: number) {
    //     this.userService.delete(id)
    //         .pipe(first())
    //         .subscribe(() => this.loadAllUsers());
    // }

    private loadAllAudits() {
        this.auditService.getAll()
            .pipe(first())
            .subscribe(auditsList => this.auditsList = auditsList);
    }
}