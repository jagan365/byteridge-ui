﻿import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

import { AuthenticationService } from './_services';
import { User } from './_models';

import './_content/app.less';

@Component({ selector: 'app', templateUrl: 'app.component.html' })
export class AppComponent {
    currentUser: User;

    constructor(
        private router: Router,
        private authenticationService: AuthenticationService,
        private http: HttpClient
    ) {
        this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
    }

    logout() {
        this.http.post(`${config.apiUrl}/users/logout`,{}).subscribe((res)=>{
            this.authenticationService.logout();
            this.router.navigate(['/login']);
          },(err)=>{
            console.log(`error`,err)
            window.alert(err.error.message)
          },()=>{
            // finally block
          })
        
    }
}